#exercice 1 : déclarer différents types de variables.

prenom = "pierre"
age= 28
majeur = True
amis = ["marie","julien","adrien"]
compte_en_banque = 20135.384
parents = ("Marc","Caroline")

print(prenom)
print(age)
print(majeur)
print(compte_en_banque)
print(amis)
print(parents)

#exercice 2 :
site_web = "google"
print(site_web)

#exercice 3 :

x = "17"

print( "Le nombre est ", x )
#solution alternative
print("le nombre est "+str(x))

#Exercice 4

a = 3
b = 6
a = b
b = 7

print(a)

#Exercice 5

a=2
b=6
c=3

print(a,b,c, sep=" + ")

#Exercice 6

list1 = range(3)
list2 = range(5)
print(list(list1)) #list est un mot réservé

#Exercice 7
prenom = "Jack"
#condition
if isinstance(prenom, str):
    print("la variable est bien une chaine de caractere")
else:
    print("la variable n'est pas une chaine de caractere")

prenom = 0
if isinstance(prenom, int):
    print("la variable est bien un int")
else:
    print("not a int")



#exercice 8

phrase="salut les dev, salut alors ça va"
phrase=phrase.replace("salut","bonsoir", 1)
print(phrase)
"""
commentaire python

Titre
Nom du projet
DAte de la dernière révision 
Auteur
IDE
Client 

##############################
#Programme Principal
##############################

zone des imports
zone déclaration des variables globales
zone déclaration modules et fonctions
Programme
"""