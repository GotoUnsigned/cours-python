#python machine learning

#lib IA :
# PyBrain / NumPy
# PyDatalog
# Simple IA
# GraphLab

#fonctionnalité de programmation "logique" et prédicats

#Haskell, lips ou Erlang ?

#Deep Blue ?
#Jeu de Go (AlphaGo en 2016)
#Test de Turing
#Eliza (implémenté en python)
#Alice bot
#Logique
#Algorithme génétique
#Data mining
#Réseaux de neuronnes
#Apprentissage automatique
#KPI Serveur - graph python
#DATA TYPES
#str (texte)
#int, float, complex ( numérique )
#list, tuple, range (Séquences)
#dict (Mapping)
#Bool (Booléen)
#bytes, bytearray, memoryview (binary)
#NoneType ( Type non)

#Vérifier le type d'une variable
#x = 5
#print(type(x))

#spécifier le type :

#x = int(20)

#x = ["pomme", "banana", "cerise"]
#print(x)
#print(type(x))

#x = tuple(("pomme","banana","cerise"))

#x = 3 + 5j
#print(type(x))

#conversion
#x = float(1)
#y = int(2.8)
#z = complex(1)

#print(x)
#print(y)
#print(z)

#a = float(x)
#b = int(y)
#c = complex(z)

#print(type(a))
#print(type(b))
#print(type(c))


#exercice
#Afficher un nombre aléatoire

import random
a = random.randint(0,10)
print(a)

#for b in "banane":
#    print(b)

#afficher la longueur de la chaine "lait"
x = "lait"
print(len(x))

#J'ai une phrase, que je souhaite détecter un mot dans cette phrase

a = "voila la phrase"
print("la" in a)

#j'aimerai afficher quelque chose de plus sympa qu'un True ou Flase.....

a = "Voila la phrase"
if "la" not in a:
        print("pasTrouvé")

#le "slicing"
a = "Exemple"
print(a[2:4])

#recuppérer un seul
print(a[3])

#afficher pl en partant à l'enver
print(a[-3:-1])

print("")

#print(bool(None))
#print(bool(0))
#print(bool(False))
#print(bool(""))

#===========================


def maFonction():

    return True


print(maFonction())

#===========================

#Probleme : J'aimerai tester si un nombre est entier....

def testFonction(nombre):
    print(isinstance(nombre, int))


testFonction(25)

x=100

print(isinstance(x, int))


#nomenclature et présentation d'un code / Projet

#tuple python ?

#permet de créer une collection ordonnée de valeurs.
#on ne peut pas modifier un tuple une fois qu'il est créer (immuable)

#a=(3,4,7)
#type(a)

#b,c=(5,6)
#(b,c)=(5,6)

#def test():
#    return 5,6

#a=test()
#(5,6)

#for i in a:
 #   print(i)

print(a[0])
print(a[1])

b = (3,)

#récupérer l'élément unique présent dans le tuple
#approche 1
c = b[0]
print(c)

#approche 2
d, = b
print(d)

#autre synthaxe nom_variable, =
