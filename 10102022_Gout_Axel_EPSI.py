#if 5>2:
#    print("cinq est inférieur à 2") #hashtagcomment

#x = 5
#y = "John"
#c= "John"
#print(y is c) #print(y==c)

#SEPARATEUR \
    
#print(3+2-3.2 \
 #   +3-15/26 \
  #      -15+3.2)
  
#NOTATION  

#variable_1=10
#Variable_1=20

#il faut éviter de mettre un majuscule au nom de la variable

#somme = variable_1 + variable_2

#ma_variable_1 = False
#ma_variable_2 = True
#print(ma_variable_1)
#print(ma_variable_2)
#ma_variable_2, ma_variable_1 = ma_variable_1, ma_variable_2
#print(ma_variable_1)
#print(ma_variable_2)
#print(type(ma_variable_1))

#elements pour afficher une chaine 

#ma_chaine_2 = 'Exemple'
#ma_chaine_3 = "Exemple"
#ma_chaine_4 = '''Exemple'''
#ma_chaine_5 = """Exemple"""
#print(ma_chaine_2,ma_chaine_3,ma_chaine_4,ma_chaine_5)
#ma_chaine_6 = "J'aime le C"
#ma_chaine_7 = "J\'aime le C"
#print(ma_chaine_6,ma_chaine_7)

#Convertisseur

#ma_variable_1 = bin(23)
#print(ma_variable_1)

#Exercice
#Livrer le fichier du jour à artzvincent@gmail.com (lien,fichier ,ect...)
#Le nom du fichier 
#Date_nom_prenom_EPSI.py